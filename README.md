# WikidataNet

This is a simple .NET wrapper for Wikidata API. So far it's a project with unstable interface
and you, as a general public :-), should not use it for nothing more than quick scripting.
In future, I want to stabilize this a little more and implement WDQ support that would greatly
increase power of the library.

## Searching

```C#
using WikidataNet;

public async Task<string> DescriptionOfPrague()
{
  var query = new WikidataSearchQuery() { Query = "prague", Limit = 10 };

	var results = await query.Run();
	var pragueEntry = results.First(r => r.Label == "Prague");
	
	return pragueEntry.Description;
}
```
