﻿using System.Net;
using System.Threading.Tasks;

namespace WikidataNet
{
	public abstract class WikidataQuery<T>
	{
		public WikidataQuery(RequestConfig requestConfig = null)
		{
			this.RequestConfig = requestConfig ?? new RequestConfig();
		}

		public RequestConfig RequestConfig { get; private set; }

		public abstract Task<T> Run();

		protected WebClient GetWebClient()
		{
			var wc = new WebClient();
			wc.Headers.Add("User-Agent", this.RequestConfig.UserAgent);
			return wc;
		}
	}
}
