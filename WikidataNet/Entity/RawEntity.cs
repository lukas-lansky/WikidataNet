﻿using System.Collections.Generic;

namespace WikidataNet.Entity
{
	public class RawEntities
	{
        public class RawEntity
        {
            public class RawClaim
            {
                /*
"mainsnak": {
    "snaktype": "value",
    "property": "P1368",
    "datavalue": 

    {
        "value": "000057405",
        "type": "string"
    },
    "datatype": "string"
},
                */

                public class RawSnak
                {
                    public class DataValue
                    {
                        public dynamic value { get; set; }

                        public string type { get; set; }
                    }

                    public string snaktype { get; set; }

                    public string property { get; set; }

                    public string datatype { get; set; }

                    public DataValue datavalue { get; set; }
                }

                public string type { get; set; }

                public string id { get; set; }

                public string rank { get; set; }

                public RawSnak mainsnak { get; set; }
            }

			public int pageid { get; set; }

			public string title { get; set; }

			public long lastrevid { get; set; }

			public string id { get; set; }

			public string type { get; set; }

            public Dictionary<string, List<RawClaim>> claims { get; set; }
		}

		public Dictionary<string, RawEntity> entities { get; set; }
        
		public int success { get; set; }
	}
}
