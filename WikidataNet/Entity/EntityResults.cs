﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WikidataNet.Entity
{
	public sealed class EntityResults : Response, IEnumerable<WikidataEntity>
	{
		private List<WikidataEntity> _entities { get; set; }

		public EntityResults(IEnumerable<WikidataEntity> entities)
		{
			this._entities = entities.ToList();
		}

		public IEnumerator<WikidataEntity> GetEnumerator()
		{
			foreach (var entity in this._entities)
			{
				yield return entity;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}
}
