﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WikidataNet.Property;

namespace WikidataNet.Entity
{
	public sealed class WikidataEntityQuery : WikidataQuery<EntityResults>
	{
		public WikidataEntityQuery(RequestConfig requestConfig = null)
			: base(requestConfig)
		{
			
		}

		public IEnumerable<string> Ids { get; set; }
		

		public override async Task<EntityResults> Run()
		{
			if (this.Ids == null || !this.Ids.Any())
			{
				throw new Exception("You should provide Ids if you want to run entity query.");
			}

			var rawEntities = await this.GetSearchResult(this.Ids);
			var entities = rawEntities.entities.Select(entityKv =>
            {
                var properties = new List<WikidataProperty>();
                foreach (var claimKv in entityKv.Value.claims)
                {
                    var id = Convert.ToInt32(claimKv.Key.Substring(1));
                    properties.Add(new WikidataProperty(id));
                }

                return new WikidataEntity(entityKv.Key, entityKv.Value.lastrevid, DateTime.MinValue, properties);
            });

			return new EntityResults(entities) { Metadata = new ResponseMetadata() };
		}

		private async Task<RawEntities> GetSearchResult(IEnumerable<string> ids)
		{
			var url = $"{this.RequestConfig.UrlBase}?action=wbgetentities&ids={string.Join(",", ids)}&format=json";
            var jsonString = await this.GetWebClient().DownloadStringTaskAsync(url);

			var json = JsonConvert.DeserializeObject<RawEntities>(jsonString);

			return json;
		}
	}
}
