﻿using System;
using System.Collections.Generic;
using WikidataNet.Property;

namespace WikidataNet.Entity
{
	public class WikidataEntity
	{
		public string Id { get; private set; }

		public long Revision { get; private set; }

		public DateTime Timestamp { get; private set; }

        public IEnumerable<WikidataProperty> Claims { get; private set; }

		public WikidataEntity(string id, long revision, DateTime timestamp, IEnumerable<WikidataProperty> claims)
		{
            if (claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

			this.Id = id;
			this.Revision = revision;
			this.Timestamp = timestamp;
            this.Claims = claims;
		}
	}
}
