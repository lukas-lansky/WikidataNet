﻿namespace WikidataNet
{
	public enum ResponseStatusEnum
	{
		Success = 1,
		SuccessWithWarnings = 2,
		Error = 2
	}
}
