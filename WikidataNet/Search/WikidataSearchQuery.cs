﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WikidataNet.Search
{
	/// <summary>
	/// Class that represents search query that can be
	/// send to Wikidata using Run method.
	/// </summary>
    public sealed class WikidataSearchQuery : WikidataQuery<SearchResults>
    {
        public WikidataSearchQuery(RequestConfig requestConfig = null)
			: base(requestConfig)
        {
            this.Limit = 7;
        }

		/// <summary>
		/// The query.
		/// </summary>
        public string Query { get; set; }

		/// <summary>
		/// Number of results. API supports any number in range [1, 50],
		/// more results in warning.
		/// </summary>
        public int Limit { get; set; }
		
        public override async Task<SearchResults> Run()
        {
            if (string.IsNullOrWhiteSpace(this.Query))
            {
                throw new Exception("Query should not be empty.");
            }

			var rawResult = await this.GetSearchResult(this.Query, this.Limit);
			var searchList = (IEnumerable<dynamic>)rawResult.search;
			var typedResults = searchList.Select(
				r => new Match(
					r.id?.ToString(),
					r.url?.ToString(),
					r.label?.ToString(),
					r.description?.ToString(),
					new List<string>()));

            return new SearchResults(typedResults, this.Query) { Metadata = new ResponseMetadata(rawResult) };
        }

		private async Task<dynamic> GetSearchResult(string query, int limit)
		{
			var url = $"{this.RequestConfig.UrlBase}?action=wbsearchentities&search={this.Query}&language=en&format=json&limit={this.Limit}";

            var jsonString = await this.GetWebClient().DownloadStringTaskAsync(url);
			var json = JsonConvert.DeserializeObject<dynamic>(jsonString);

			return json;
		}
	}
}
