﻿using System.Collections.Generic;

namespace WikidataNet.Search
{
	/// <summary>
	/// Representation of a single search match.
	/// </summary>
    public class Match
    {
		public Match(string id, string url, string label, string description, IEnumerable<string> aliases)
		{
			this.Id = id;
			this.Url = url;
			this.Label = label;
			this.Description = description;
			this.Aliases = aliases;
		}

        public string Id { get; private set; }

        public string Url { get; private set; }

        public string Label { get; private set; }

        public string Description { get; private set; }

		public IEnumerable<string> Aliases { get; private set; }
    }
}
