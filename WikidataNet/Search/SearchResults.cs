﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WikidataNet.Search
{
	/// <summary>
	/// Represents a list of matches to past query.
	/// </summary>
    public class SearchResults : Response, IEnumerable<Match>
    {
		/// <summary>
		/// Number of returned results, not the total number of matches.
		/// </summary>
        public int Count
        {
            get
            {
                return this._results.Count;
            }
        }

		/// <summary>
		/// Original query that was used to produce this result.
		/// </summary>
        public string Query { get; private set; }

        private List<Match> _results;
        
        public SearchResults(IEnumerable<Match> results, string query)
        {
            this._results = results.ToList();
            this.Query = query;
        }

        public IEnumerator<Match> GetEnumerator()
        {
            foreach (var sr in this._results)
            {
                yield return sr;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
