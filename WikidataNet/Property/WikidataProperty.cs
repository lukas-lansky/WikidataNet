﻿namespace WikidataNet.Property
{
    public class WikidataProperty
    {
        public int Id { get; private set; }

        public int ReferingEntityId { get; private set; }

        public WikidataProperty(int id)
        {
            this.Id = id;
        }
    }
}
