﻿namespace WikidataNet.Utils
{
	/// <summary>
	/// Small subset of useful properties for more readable query construction.
	/// </summary>
	public enum KnownProperty
	{
        InstanceOf = 31,
		EducatedAt = 69
	}
}
