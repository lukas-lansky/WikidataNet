﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace WikidataNet
{
	public class ResponseMetadata
	{
		public ResponseMetadata()
		{
			this.Status = ResponseStatusEnum.Success;
			this.Warnings = new Dictionary<string, string>();
		}

		public ResponseMetadata(ResponseStatusEnum status, IDictionary<string, string> warnings)
		{
			this.Status = status;
			this.Warnings = warnings ?? new Dictionary<string, string>();
		}

		public ResponseMetadata(dynamic responseJson)
		{
			var success = (string)responseJson.success?.ToString();

			if (success == "1")
			{
				if (responseJson.warnings == null)
				{
					this.Warnings = new Dictionary<string, string>();
					this.Status = ResponseStatusEnum.Success;

					return;
				}
				else
				{
					// ugly as hell, TODO

					var jsonString = JsonConvert.SerializeObject(responseJson.warnings);
					IDictionary<string, IDictionary<string, string>> typedWarnings = JsonConvert.DeserializeObject<IDictionary<string, IDictionary<string, string>>>(jsonString);
					
					this.Warnings = typedWarnings[typedWarnings.Keys.First()];
					this.Status = ResponseStatusEnum.SuccessWithWarnings;
				}
			}
			else
			{
				this.Status = ResponseStatusEnum.Error;
				this.Warnings = new Dictionary<string, string>();
			}
		}

		/// <summary>
		/// Is everything OK?
		/// </summary>
		public ResponseStatusEnum Status { get; private set; }

		/// <summary>
		/// List of warning messages.
		/// </summary>
		public IDictionary<string, string> Warnings { get; private set; }
	}
}
