﻿namespace WikidataNet
{
	public class RequestConfig
	{
		public string UrlBase { get; private set; }

		public string UserAgent { get; private set; }

		public RequestConfig(string userAgent = null, string urlBase = null)
		{
			this.UrlBase = urlBase ?? "https://www.wikidata.org/w/api.php";
			this.UserAgent = userAgent ?? "WikidataNet library";
		}
	}
}
