﻿namespace WikidataNet
{
	public abstract class Response
	{
		/// <summary>
		/// Were there any errors? Warnings?
		/// </summary>
		public ResponseMetadata Metadata { get; set; }
	}
}
