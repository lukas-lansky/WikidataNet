﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WikidataNet.WDQ;
using WikidataNet.Utils;
using System.Threading.Tasks;
using WikidataNet.WDQ.Query;

namespace WikidataNet.Test
{
	[TestClass]
	public class WdqTest
	{
		[TestMethod]
		public async Task BolzanoStudiedAtCharlesUniversity()
		{
			var charlesUniversityId = 31519;
			var bolzanoId = 184735;

			var query = new SingleClaimSpecification(KnownProperty.EducatedAt, charlesUniversityId);
			var result = await new WdqRequest { Query = query }.Run();

			Assert.IsTrue(result.Any(id => id == bolzanoId));
        }
        
        [TestMethod]
        public async Task UsPlacesNamedAfterFrancisOfAssisi()
        {
            var query = new RawQuerySpecification("TREE[30][150][17,131] AND CLAIM[138:676555]");
            var result = await new WdqRequest { Query = query }.Run();

            Assert.IsTrue(result.Any(id => id == 62));
        }
	}
}
