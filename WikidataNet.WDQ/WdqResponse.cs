﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using WikidataNet.Entity;
using System.Threading.Tasks;

namespace WikidataNet.WDQ
{
    public class WdqResponse : IEnumerable<int>
    {
        public WdqRawResponse RawResponse { get; private set; }

        public WdqResponse(WdqRawResponse rawResponse)
        {
            RawResponse = rawResponse;
        }

        public IEnumerator<int> GetEnumerator()
        {
            foreach (var item in this.RawResponse.items)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public async Task<IEnumerable<WikidataEntity>> GetEntities()
        {
            return await new WikidataEntityQuery { Ids = new List<int>(this).Select(i => i.ToString()) }.Run();
        }
    }
}
