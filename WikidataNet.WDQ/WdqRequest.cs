﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using WikidataNet.WDQ.Query;

namespace WikidataNet.WDQ
{
    public class WdqRequest
    {
		public WdqRequest(WdqRequestConfig config = null)
		{
			this.Config = config ?? new WdqRequestConfig();
		}

		public WdqRequestConfig Config { get; private set; }

		public QuerySpecification Query { get; set; }
		
		public async Task<WdqResponse> Run()
		{
			var query = this.Query.ToString();
			var url = $"{this.Config.UrlBase}?q={query}";

			var wc = new WebClient();
			wc.Headers.Add("User-Agent", this.Config.UserAgent);

			var jsonString = await wc.DownloadStringTaskAsync(url);
			var rawResponse = JsonConvert.DeserializeObject<WdqRawResponse>(jsonString);

			return new WdqResponse(rawResponse);
		}
    }
}
