﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WikidataNet.WDQ
{
	/// <summary>
	/// Class to be matched against WDQ response JSON by DeserializeObject.
	/// </summary>
    public class WdqRawResponse
    {
		public class Status
		{
			public string error { get; set; }

			public int items { get; set; }

			public string querytime { get; set; }

			public string parsed_query { get; set; }
		}
		
		public Status status { get; set; }

		public IEnumerable<int> items { get; set; }
	}
}
