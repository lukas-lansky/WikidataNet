﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WikidataNet.WDQ
{
    public class WdqRequestConfig
    {
		public string UrlBase { get; private set; }

		public string UserAgent { get; private set; }

		public WdqRequestConfig(string userAgent = null, string urlBase = null)
		{
			this.UrlBase = urlBase ?? "http://wdq.wmflabs.org/api";
			this.UserAgent = userAgent ?? "WikidataNet library";
		}
	}
}
