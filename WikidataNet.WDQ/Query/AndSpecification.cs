﻿using System.Collections.Generic;
using System.Linq;

namespace WikidataNet.WDQ.Query
{
	public class AndSpecification : QuerySpecification
	{
		public IEnumerable<QuerySpecification> Subqueries { get; private set; }
		
		public AndSpecification(params QuerySpecification[] subqueries)
		{
			this.Subqueries = subqueries;
		}

		public override string ToString()
		{
			return string.Join(" AND ", this.Subqueries.Select(sq => $"({sq})"));
		}
	}
}
