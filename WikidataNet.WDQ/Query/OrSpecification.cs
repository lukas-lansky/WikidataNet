﻿using System.Collections.Generic;
using System.Linq;

namespace WikidataNet.WDQ.Query
{
	public class OrSpecification : QuerySpecification
	{
		public IEnumerable<QuerySpecification> Subqueries { get; private set; }
		
		public OrSpecification(params QuerySpecification[] subqueries)
		{
			this.Subqueries = subqueries;
		}

		public override string ToString()
		{
			return string.Join(" OR ", this.Subqueries.Select(sq => $"({sq})"));
        }
	}
}
