﻿namespace WikidataNet.WDQ.Query
{
    public class RawQuerySpecification : QuerySpecification
    {
        public readonly string RawQuery;

        public RawQuerySpecification(string rawQuery)
        {
            RawQuery = rawQuery;
        }

        public override string ToString()
        {
            return RawQuery;
        }
    }
}
