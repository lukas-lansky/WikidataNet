﻿using WikidataNet.Utils;

namespace WikidataNet.WDQ.Query
{
	public class SingleClaimSpecification : QuerySpecification
	{
		public int Property { get; private set; }

		public int? Item { get; private set; }

		public QuerySpecification Subquery { get; private set; }

		public SingleClaimSpecification(int property, int? item = null)
		{
			this.Property = property;
			this.Item = item;
		}

		public SingleClaimSpecification(int property, QuerySpecification subquery)
		{
			this.Property = property;
			this.Subquery = subquery;
		}

		public SingleClaimSpecification(KnownProperty property, int? item = null) : this((int)property, item) { }

		public SingleClaimSpecification(KnownProperty property, QuerySpecification subquery) : this((int)property, subquery) { }

		public override string ToString()
		{
			if (this.Item.HasValue)
			{
				return $"claim[{this.Property}:{this.Item.Value}]";
			}
			else if (this.Subquery != null)
			{
				return $"claim[{this.Property}:(${this.Subquery.ToString()})]";
            }
			else
			{
				return $"claim[{this.Property}]";
			}
		}

		public string ToStringWithoutEnvelope()
		{
			if (this.Item.HasValue)
			{
				return $"{this.Property}:{this.Item.Value}";
			}
			else if (this.Subquery != null)
			{
				return $"{this.Property}:(${this.Subquery.ToString()})";
			}
			else
			{
				return $"{this.Property}";
			}
		}
	}
}
