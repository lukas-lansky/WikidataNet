﻿using System.Collections.Generic;

namespace WikidataNet.WDQ.Query
{
	/// <summary>
	/// Union of results of SingleClaimSpecification.
	/// </summary>
	public class ClaimSpecification : QuerySpecification
	{
		public IEnumerable<SingleClaimSpecification> Claims { get; private set; }

		public ClaimSpecification(IEnumerable<SingleClaimSpecification> claims)
		{
			this.Claims = claims;
		}

		public override string ToString()
		{
			return $"claim[{string.Join(",", this.Claims)}]";
        }
	}
}
