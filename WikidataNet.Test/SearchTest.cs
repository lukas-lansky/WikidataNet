﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using WikidataNet.Search;

namespace WikidataNet.Test
{
	[TestClass]
	public class SearchTest
	{
		/// <summary>
		/// Quite brittle test that checks whether Prague looks like it
		/// looked like in 2015/4. :-) TODO.
		/// </summary>
		[TestMethod]
		public async Task PragueSearchWorks()
		{
			var query = new WikidataSearchQuery() { Query = "prague", Limit = 10 };

			var results = await query.Run();

			Assert.AreEqual(results.Metadata.Status, ResponseStatusEnum.Success);
			Assert.IsFalse(results.Metadata.Warnings.Any());

			Assert.IsTrue(results.Any(r => r.Id == "Q1085"));

			var prague = results.First(r => r.Id == "Q1085");

			Assert.AreEqual(prague.Label, "Prague");
            Assert.AreEqual(prague.Url, "//www.wikidata.org/wiki/Q1085");
			Assert.AreEqual(prague.Description, "capital city of the Czech Republic");
			Assert.IsFalse(prague.Aliases.Any());
        }

		/// <summary>
		/// SearchResults.Query should work.
		/// </summary>
		[TestMethod]
		public async Task SearchSavesQuery()
		{
			var queryString = "prague";
			var query = new WikidataSearchQuery() { Query = queryString, Limit = 10 };

			var results = await query.Run();

			Assert.AreEqual(results.Query, queryString);
		}

		/// <summary>
		/// Basically testing whether warnings are parsed properly.
		/// </summary>
		[TestMethod]
		public async Task BigLimitThrowsVisibleWarning()
		{
			var queryString = "prague";
			var query = new WikidataSearchQuery() { Query = queryString, Limit = 100 };

			var results = await query.Run();

			Assert.AreEqual(results.Metadata.Status, ResponseStatusEnum.SuccessWithWarnings);
			Assert.IsTrue(results.Metadata.Warnings.Any());
		}
	}
}
