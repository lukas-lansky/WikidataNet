﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using WikidataNet.Entity;

namespace WikidataNet.Test
{
	[TestClass]
	public class EntityTest
	{
		[TestMethod]
		public async Task BasicEntityGet()
		{
			var entity = (await new WikidataEntityQuery() { Ids = new string[] { "Q42" } }.Run()).First();

			Assert.AreEqual(entity.Id, "Q42");
			Assert.IsTrue(entity.Revision >= 206058646);
            Assert.IsTrue(entity.Claims.Any(c => c.Id == 31));
		}
	}
}
